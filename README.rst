xrstf/ip-utils
==============

This repo was cloned from mercurial repository on ``xrstf/ip-utils``.

Since original sources on https://bitbucket.org/xrstf/ip-utils are now dead but packages currently still use it as dependency,
you can add following to your ``composer.json``:

.. sourcecode:: yaml

    {
        "repositories" : [
            {
                "type": "vcs",
                "url": "https://bitbucket.com/glorpen/ip-utils"
            }
        ]
    }

And then ``composer update xrstf/ip-utils --ignore-platform-reqs`` to update ``composer.lock``.
